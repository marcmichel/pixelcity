//
//  CollectionViewCell.swift
//  pixelcity
//
//  Created by Marc Michel on 9/16/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
