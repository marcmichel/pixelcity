//
//  File.swift
//  pixelcity
//
//  Created by Marc Michel on 9/16/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

typealias Completion = (_ Success: Bool) -> ()

let CELL_IDENTIFIER = "photoCell"

let API_KEY = "683b60e2ac0b6592083db317e4701d46"



func setUPApi(forAPI for: String, foCcoordinate coordinate: DropPin, for numberOfPhotos: Int) -> String {
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(API_KEY)&lat=\(coordinate.coordinate.latitude)&lon=\(coordinate.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(numberOfPhotos)&format=json&nojsoncallback=1"
}
