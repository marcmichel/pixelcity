//
//  PopVC.swift
//  pixelcity
//
//  Created by Marc Michel on 9/18/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import Alamofire

class PopVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewLabel: UIView!
    
    var passImage: UIImage!


    
    func initData(forImage image: UIImage){
        self.passImage = image
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = passImage
        viewLabel.layer.cornerRadius = 10
        tapDismiss()
    }
    
    func tapDismiss() {
        let tapDismiss = UITapGestureRecognizer()
        tapDismiss.numberOfTapsRequired = 2
        tapDismiss.delegate = self
        tapDismiss.addTarget(self, action: #selector(dismissVC))
        view.addGestureRecognizer(tapDismiss)
    }
    
    func dismissVC() {
        dismiss(animated: true, completion: nil)
    }
    

}
