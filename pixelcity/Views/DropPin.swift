//
//  DropPin.swift
//  pixelcity
//
//  Created by Marc Michel on 9/13/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import MapKit

class DropPin: NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    var idendifier: String!
    
    init(coordinate: CLLocationCoordinate2D, identifier: String) {
        self.coordinate = coordinate
        self.idendifier = identifier
        super.init()
    }
}
