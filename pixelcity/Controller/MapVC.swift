//
//  ViewController.swift
//  pixelcity
//
//  Created by Marc Michel on 9/13/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MapKit
import CoreLocation

class MapVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var pullUpView: UIView!
    @IBOutlet weak var pullUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    let AuthStatus = CLLocationManager.authorizationStatus()
    let radius: Double = 1000
    
    var spinner: UIActivityIndicatorView?
    var label: UILabel?
    var screenSize = UIScreen.main.bounds
    var collection: UICollectionView?
    var floLayout = UICollectionViewFlowLayout()
    var imageURLArray = [String]()
    var imageArray = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationManager.delegate = self
        updatelocation()
        configureLocationServices()
        pullUpView.layer.cornerRadius = 5
        dubletap()
        
        collection = UICollectionView(frame: view.bounds, collectionViewLayout: floLayout)
        collection?.register(CollectionViewCell.self, forCellWithReuseIdentifier: CELL_IDENTIFIER)
        collection?.delegate = self
        collection?.dataSource = self
        collection?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        collection?.layer.cornerRadius = 5
        pullUpView.addSubview(collection!)
        
        
    }
    
   
    func removePin() {
        for annotation in mapView.annotations {
            mapView.removeAnnotation(annotation)
        }
    }
    
    func removeSpinner() {
        if spinner != nil {
            spinner?.removeFromSuperview()
        }
    }
    
    
    func updatelocation() {
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.startUpdatingLocation()
    }
    
    func dubletap() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(dropPin(sender:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        mapView.addGestureRecognizer(doubleTap)
    }
    
    func addSwipe() {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(animateViewDown))
        swipe.direction = .down
        pullUpView.addGestureRecognizer(swipe)
    }
    
    func animateView() {
        pullUpViewHeightConstraint.constant = 300
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func animateViewDown() {
        cancellAllsession()
        pullUpViewHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func spinnerAnimation() {
        spinner = UIActivityIndicatorView()
        spinner?.activityIndicatorViewStyle = .whiteLarge
        spinner?.color = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        spinner?.center = CGPoint(x: (screenSize.width / 2) - ((spinner?.frame.width)! / 2), y: 150)
        spinner?.startAnimating()
        collection?.addSubview(spinner!)
    }
    
    func labelText() {
        label = UILabel()
        label?.text = "25/50 PHOTO LOADING..."
        label?.textAlignment = .center
        label?.font = UIFont(name: "Avenir Next", size: 14)
        label?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        label?.frame = CGRect(x: (screenSize.width / 2) - 120, y: 175, width: 240, height: 40)
        collection?.addSubview(label!)
    }
    
    func removeLabel() {
        if label != nil {
            label?.removeFromSuperview()
        }
    }
    
    func dropPin(sender: UITapGestureRecognizer) {
        removePin()
        removeSpinner()
        removeLabel()
        cancellAllsession()
        animateView()
        addSwipe()
        spinnerAnimation()
        labelText()

        let touchPoint = sender.location(in: mapView)
        let touchCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let annotation = DropPin(coordinate: touchCoordinate, identifier: "dropPin")
        mapView.addAnnotation(annotation)
        let coordinationregion = MKCoordinateRegionMakeWithDistance(touchCoordinate, radius * 2.0, radius * 2.0)
        mapView.setRegion(coordinationregion, animated: true)
        getImage(forAnnotion: annotation) { (finished) in
            if finished {
                self.retrieveImage(completion: { (finished) in
                    if finished {
                        self.removeSpinner()
                        self.removeLabel()
                        self.collection?.reloadData()
                    }
                })
            }
        }
    }

    @IBAction func mapButton(_ sender: Any) {
        if AuthStatus == .authorizedAlways || AuthStatus == .authorizedWhenInUse {
            centerMap()
        }
    }
    
    func getImage(forAnnotion annotation: DropPin, completion: @escaping Completion) {
        imageURLArray = []
        Alamofire.request(setUPApi(forAPI: API_KEY, foCcoordinate: annotation, for: 40)).responseJSON { (response) in
            guard let json = response.result.value as? Dictionary<String, AnyObject> else {return}
            let photoDict = json["photos"] as! Dictionary<String, AnyObject>
            let photoArray = photoDict["photo"] as! [Dictionary<String, AnyObject>]
            for photo in photoArray {
                let postURL = "https://farm\(photo["farm"]!).staticflickr.com/\(photo["server"]!)/\(photo["id"]!)_\(photo["secret"]!)_h_d.jpg"
                self.imageURLArray.append(postURL)
            }
            completion(true)
        }
    }
    
    func retrieveImage(completion: @escaping Completion) {
        imageArray = []
        for url in imageURLArray {
            Alamofire.request(url).responseImage { (response) in
                guard let image = response.result.value else {return}
                self.imageArray.append(image)
                self.label?.text = "\(self.imageArray.count)/40 IMAGES DOWNLOADED"
                if self.imageArray.count == self.imageURLArray.count {
                    completion(true)
                }
            }
        }
    }
    
    func cancellAllsession() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach({ $0.cancel() })
            downloadData.forEach({ $0.cancel() })
        }
    }
}

extension MapVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let pinAnnotaion = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "dropPin")
        pinAnnotaion.pinTintColor = #colorLiteral(red: 0.9771530032, green: 0.7062081099, blue: 0.1748393774, alpha: 1)
        pinAnnotaion.animatesDrop = true
        return pinAnnotaion
    }
    func centerMap() {
        guard let coordinate = locationManager.location?.coordinate else {return}
        let region = MKCoordinateRegionMakeWithDistance(coordinate, radius * 2.0, radius * 2.0)
        mapView.setRegion(region, animated: true)
    }
}

extension MapVC: CLLocationManagerDelegate {
    func configureLocationServices() {
        if AuthStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else {return}
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMap()
    }
}

extension MapVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER, for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
        let imageFromIndex = imageArray[indexPath.row]
        let imageView = UIImageView(image: imageFromIndex)
        cell.addSubview(imageView)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopVC") as? PopVC else {return}
        popVC.initData(forImage: imageArray[indexPath.row])
        present(popVC, animated: true, completion: nil)
    }
    
}


